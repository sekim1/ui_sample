package com.dtwocorp.citroorder.uitest.ProductDetail.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOption;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOptionItem;
import com.dtwocorp.citroorder.uitest.ProductDetail.view.ProductDetailActivity;
import com.dtwocorp.citroorder.uitest.R;

import java.util.ArrayList;

public class CheckboxAdapter extends RecyclerView.Adapter<CheckboxAdapter.ViewHolder> {

    private ArrayList<ProductOptionItem> optionItems;
    private ProductOption menuOption;
    private ArrayList<Integer> selectedOptions;
    private ProductDetailActivity activity;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context parentContext = parent.getContext();
        LayoutInflater inflater = (LayoutInflater) parentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate( R.layout.layout_option_item_button, parent, false );
        CheckboxAdapter.ViewHolder viewHolder = new CheckboxAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try{
            ProductOptionItem optionItem = optionItems.get(position);

            String name = optionItem.getName();
            int price = optionItem.getPrice();
            int id = optionItem.getId();

            holder.tvName.setText(name);
            holder.tvPrice.setText(String.valueOf(  price ));

            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("============checkbox click", name + id );
                }
            });



        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return optionItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout container;
        private TextView tvName, tvPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            container = itemView.findViewById( R.id.container_option_checkbox_item );
            tvName = itemView.findViewById(R.id.tv_option_checkbox_name);
            tvPrice = itemView.findViewById(R.id.tv_option_checkbox_price);
        }
    }

    public CheckboxAdapter(ProductDetailActivity activity , ProductOption menuOption){
        this.activity = activity;
        this.menuOption = menuOption;
        this.optionItems = menuOption.getOptionItems();
    }
}
