package com.dtwocorp.citroorder.uitest.API.Category;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CategoryAPI {

    @GET("stores/{storeId}/categories")
    Call<CategoryResponseBody> getCategories(@Path("storeId") int storeId );

    @GET("stores/{storeId}/categories/{categoryId}/products")
    Call<CategoryProductResponseBody> getCategoryProducts(@Path("storeId") int storeId, @Path("categoryId") int categoryId );

}
