package com.dtwocorp.citroorder.uitest.ProductDetail.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.dtwocorp.citroorder.uitest.API.ProductDetail.ProductAPI;
import com.dtwocorp.citroorder.uitest.API.ProductDetail.ProductDetailResponseBody;
import com.dtwocorp.citroorder.uitest.API.RetrofitBuilder;
import com.dtwocorp.citroorder.uitest.ProductDetail.adapter.OptionListAdapter;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.AddOptionInfo;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductInfo;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOption;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOptionItem;
import com.dtwocorp.citroorder.uitest.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ProductDetailActivity extends AppCompatActivity {

    ArrayList<ProductOption> options = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        //setData();

        RecyclerView recyclerView = findViewById(R.id.recycler_product_options);

        Intent getIntent = getIntent();

        int storeId = getIntent.getIntExtra("storeId", -1 );
        int categoryId = getIntent.getIntExtra("categoryId", -1 );
        int productId = getIntent.getIntExtra("productId", -1 );

        if( storeId != -1 && categoryId != -1 && productId != -1 ){
            try{
                Retrofit retrofit = RetrofitBuilder.getRetrofitClient();
                ProductAPI productAPI = retrofit.create( ProductAPI.class );

                Call<ProductDetailResponseBody> call = productAPI.getProductDetail(2,1,1 );
                call.enqueue(new Callback<ProductDetailResponseBody>() {
                    @Override
                    public void onResponse(Call<ProductDetailResponseBody> call, Response<ProductDetailResponseBody> response) {
                        try{

                            int status = response.code();
                            Log.e("=====status" , String.valueOf(status));

                            if( status >= 200 && status < 300 ){

                                ProductDetailResponseBody body = response.body();
                                ProductInfo productInfo = body.getProductInfo();
                                options = productInfo.getOptions();

                                LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false );
                                OptionListAdapter adapter = new OptionListAdapter( ProductDetailActivity.this, options );

                                recyclerView.setLayoutManager(manager);
                                recyclerView.setAdapter( adapter );

                            }else{
//                            String error = response.errorBody().string();
//                            Log.e("=====", error);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductDetailResponseBody> call, Throwable t) {

                    }
                });

            }catch (Exception e){
                e.printStackTrace();
            }
        }





    }

    public void setData(){
        try{
            ProductOptionItem radioHot = new ProductOptionItem(3, "따뜻하게123123123", 0,0,0,0,true, "", false, false, "per_count", "");
            ProductOptionItem radioCold = new ProductOptionItem(4, "차갑게", 0,0,0,0,true, "", false, false, "per_count", "");
            ArrayList<ProductOptionItem> tempList = new ArrayList<>();
            tempList.add(radioHot);
            tempList.add(radioCold);
            tempList.add(radioHot);
            tempList.add(radioCold);
            tempList.add(radioHot);
            tempList.add(radioCold);

            ProductOption optionTemp = new ProductOption(2, "온도", "radio", false, tempList );

            ProductOption optionNumber = new ProductOption(3, "온도", "number", false, tempList );
            ProductOption optionCheckbox = new ProductOption(4, "온도", "checkbox", false, tempList );

            options.add(optionTemp);
            options.add(optionNumber);
            options.add(optionCheckbox);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void addOption(AddOptionInfo addOptionInfo ){
        try{

        }catch (Exception e ){
            e.printStackTrace();
        }
    }

    public void deleteOption( AddOptionInfo addOptionInfo  ){

    }


}