package com.dtwocorp.citroorder.uitest.API.Category;

import androidx.annotation.Nullable;

import com.dtwocorp.citroorder.uitest.PosMain.model.CategoryInfo;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryResponseBody {
    @SerializedName("status")
    private String status;

    @Nullable
    @SerializedName("message")
    private String message;

    @Nullable
    @SerializedName("data")
    private ArrayList<CategoryInfo> data;

    public String getStatus() {
        return status;
    }

    @Nullable
    public String getMessage() {
        return message;
    }

    @Nullable
    public ArrayList<CategoryInfo> getData() {
        return data;
    }
}
