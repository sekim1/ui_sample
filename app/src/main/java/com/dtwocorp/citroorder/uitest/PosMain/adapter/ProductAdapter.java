package com.dtwocorp.citroorder.uitest.PosMain.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dtwocorp.citroorder.uitest.PosMain.model.CategoryProductInfo;
import com.dtwocorp.citroorder.uitest.PosMain.view.PosMainActivity;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOption;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOptionItem;
import com.dtwocorp.citroorder.uitest.ProductDetail.view.ProductDetailActivity;
import com.dtwocorp.citroorder.uitest.R;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private ArrayList<CategoryProductInfo> productInfos;
    private PosMainActivity activity;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context parentContext = parent.getContext();
        LayoutInflater inflater = (LayoutInflater) parentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate( R.layout.layout_home_product_container, parent, false );
        ProductAdapter.ViewHolder viewHolder = new ProductAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try{
            CategoryProductInfo info = productInfos.get(position);

            int id = info.getId();
            String name = info.getName();
            int price = info.getPrice();

            holder.tvPrice.setText( String.valueOf(price) );
            holder.tvName.setText( name );

            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int storeId = activity.getStoreId();
                    int categoryId = activity.getCategoryId();
                    Log.e("======",   storeId +"  " +  categoryId  +  "    " + id +"  상세 화면으로 이동 ");
                    Intent sendIntent = new Intent( activity, ProductDetailActivity.class );
                    sendIntent.putExtra("storeId" , storeId );
                    sendIntent.putExtra("categoryId", categoryId);
                    sendIntent.putExtra("productId", id );

                    activity.startActivity( sendIntent );
                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return productInfos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout container;
        private TextView tvName, tvPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.container_home_product);
            tvName = itemView.findViewById(R.id.tv_home_product_container_name );
            tvPrice = itemView.findViewById(R.id.tv_home_product_container_price );
        }
    }

    public ProductAdapter(PosMainActivity activity , ArrayList<CategoryProductInfo> productInfos  ){
        this.activity = activity;
        this.productInfos = productInfos;
    }
}
