package com.dtwocorp.citroorder.uitest.ProductDetail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOption;
import com.dtwocorp.citroorder.uitest.ProductDetail.view.ProductDetailActivity;
import com.dtwocorp.citroorder.uitest.R;

import java.util.ArrayList;

public class OptionListAdapter extends RecyclerView.Adapter<OptionListAdapter.ViewHolder> {

    private ArrayList<ProductOption> options;
    private ProductDetailActivity activity;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context parentContext = parent.getContext();
        LayoutInflater inflater = (LayoutInflater) parentContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.layout_option_list_item, parent, false );
        OptionListAdapter.ViewHolder viewHolder = new OptionListAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try{
            ProductOption option = options.get(position);

            String type = option.getType();
            String name = option.getName();

            // type에 따라서 recylcerview adapter를 다르게 적용하기

            holder.tvTitle.setText(name);

            RecyclerView recyclerView = holder.optionItemsView;


            if( type.equals("radio") ){
                GridLayoutManager gridLayoutManager = new GridLayoutManager(activity.getApplicationContext(), 3);
                RadioAdapter radioAdapter = new RadioAdapter( activity, option );

                recyclerView.setLayoutManager( gridLayoutManager );
                recyclerView.setAdapter(  radioAdapter );
            }else if ( type.equals("checkbox")){
                GridLayoutManager gridLayoutManager = new GridLayoutManager(activity.getApplicationContext(), 3);
                CheckboxAdapter checkboxAdapter = new CheckboxAdapter(activity, option);

                recyclerView.setLayoutManager( gridLayoutManager );
                recyclerView.setAdapter(  checkboxAdapter );

            }else{ // number
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager( activity.getApplicationContext(), RecyclerView.VERTICAL, false );
                NumberAdapter numberAdapter = new NumberAdapter( activity, option );

                recyclerView.setLayoutManager( linearLayoutManager );
                recyclerView.setAdapter(  numberAdapter );
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return options.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private RecyclerView optionItemsView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById( R.id.tv_option_list_name );
            optionItemsView = itemView.findViewById(R.id.recycler_product_option_items );
        }
    }

    public OptionListAdapter(ProductDetailActivity activity, ArrayList<ProductOption> options){
        this.activity = activity;
        this.options = options;
    }
}
