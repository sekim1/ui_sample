package com.dtwocorp.citroorder.uitest.ProductDetail.model;

import com.google.gson.annotations.SerializedName;

public class ProductOptionItem {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("price")
    private int price;

    @SerializedName("init_count")
    private int initCount;

    @SerializedName("min_count")
    private int minCount;

    @SerializedName("max_count")
    private int maxCount;

    @SerializedName("init_value")
    private boolean initValue;

    @SerializedName("color_text")
    private String colorText;

    @SerializedName("use_color")
    private boolean useColor;

    @SerializedName("sold_out")
    private boolean soldOut;

    @SerializedName("price_calc_type")
    private String priceCalcType;

    @SerializedName("attachment")
    private String attachment;

    public ProductOptionItem(int id, String name, int price, int initCount, int minCount, int maxCount, boolean initValue, String colorText, boolean useColor, boolean soldOut, String priceCalcType, String attachment) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.initCount = initCount;
        this.minCount = minCount;
        this.maxCount = maxCount;
        this.initValue = initValue;
        this.colorText = colorText;
        this.useColor = useColor;
        this.soldOut = soldOut;
        this.priceCalcType = priceCalcType;
        this.attachment = attachment;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getInitCount() {
        return initCount;
    }

    public int getMinCount() {
        return minCount;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public boolean isInitValue() {
        return initValue;
    }

    public String getColorText() {
        return colorText;
    }

    public boolean isUseColor() {
        return useColor;
    }

    public boolean isSoldOut() {
        return soldOut;
    }

    public String getPriceCalcType() {
        return priceCalcType;
    }

    public String getAttachment() {
        return attachment;
    }
}
