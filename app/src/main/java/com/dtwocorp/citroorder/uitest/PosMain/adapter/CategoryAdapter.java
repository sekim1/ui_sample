package com.dtwocorp.citroorder.uitest.PosMain.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dtwocorp.citroorder.uitest.PosMain.model.CategoryInfo;
import com.dtwocorp.citroorder.uitest.PosMain.view.PosMainActivity;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOption;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOptionItem;
import com.dtwocorp.citroorder.uitest.ProductDetail.view.ProductDetailActivity;
import com.dtwocorp.citroorder.uitest.R;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

   private ArrayList<CategoryInfo> categoryInfos;
   private PosMainActivity activity;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context parentContext = parent.getContext();
        LayoutInflater inflater = (LayoutInflater) parentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate( R.layout.layout_home_category_btn , parent, false );
        CategoryAdapter.ViewHolder viewHolder = new CategoryAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try{
            CategoryInfo categoryInfo = categoryInfos.get(position);

            int id = categoryInfo.getId();
            String name = categoryInfo.getName();

            holder.btnCategory.setText(name);
            holder.btnCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.getCategoryProductList( id );
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return categoryInfos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private Button btnCategory;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            btnCategory = itemView.findViewById(R.id.btn_select_category);
        }
    }

    public CategoryAdapter( PosMainActivity activity , ArrayList<CategoryInfo> categoryInfos){
        this.activity = activity;
        this.categoryInfos = categoryInfos;
    }
}
