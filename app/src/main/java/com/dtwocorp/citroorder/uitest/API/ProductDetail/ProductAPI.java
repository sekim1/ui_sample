package com.dtwocorp.citroorder.uitest.API.ProductDetail;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ProductAPI {
    @GET("stores/{storeId}/categories/{categoryId}/products/{productId}")
    Call<ProductDetailResponseBody> getProductDetail(@Path("storeId") int storeId, @Path("categoryId") int categoryId , @Path("productId") int productId );

}
