package com.dtwocorp.citroorder.uitest.API.ProductDetail;

import androidx.annotation.Nullable;

import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductInfo;
import com.google.gson.annotations.SerializedName;

public class ProductDetailResponseBody {

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @Nullable
    @SerializedName("data")
    private ProductInfo productInfo;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    @Nullable
    public ProductInfo getProductInfo() {
        return productInfo;
    }
}
