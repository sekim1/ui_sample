package com.dtwocorp.citroorder.uitest.ProductDetail.model;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class MenuDetail {
    private int id;
    private String name;
    private int price;
    private String attachment;

    private boolean soldOut;

    @Nullable
    private int stock;

    private String content;
    private ArrayList<ProductOption> options;
}
