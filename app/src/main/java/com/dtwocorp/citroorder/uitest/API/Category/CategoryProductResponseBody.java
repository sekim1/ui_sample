package com.dtwocorp.citroorder.uitest.API.Category;

import androidx.annotation.Nullable;

import com.dtwocorp.citroorder.uitest.PosMain.model.CategoryProductInfo;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryProductResponseBody {
    @SerializedName("status")
    private String status;

    @Nullable
    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private ArrayList<CategoryProductInfo> data;

    public String getStatus() {
        return status;
    }

    @Nullable
    public String getMessage() {
        return message;
    }

    public ArrayList<CategoryProductInfo> getData() {
        return data;
    }
}
