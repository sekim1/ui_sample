package com.dtwocorp.citroorder.uitest.ProductDetail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOption;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductOptionItem;
import com.dtwocorp.citroorder.uitest.ProductDetail.view.ProductDetailActivity;
import com.dtwocorp.citroorder.uitest.R;

import java.util.ArrayList;

public class NumberAdapter extends  RecyclerView.Adapter<NumberAdapter.ViewHolder> {

    private ArrayList<ProductOptionItem> optionItems;
    private ProductOption menuOption;
    private ArrayList<Integer> selectedOptions;
    private ProductDetailActivity activity;

    private int count;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context parentContext = parent.getContext();
        LayoutInflater inflater = (LayoutInflater) parentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate( R.layout.layout_option_number, parent, false );
        NumberAdapter.ViewHolder viewHolder = new NumberAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try{
            ProductOptionItem item = optionItems.get(position);

            count = item.getInitCount();
            String name = item.getName();
            int price = item.getPrice();

            holder.tvTitle.setText( name );
            holder.tvCount.setText( String.valueOf(count) );
            holder.tvPrice.setText( String.valueOf(price) );

            holder.btnPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            holder.btnMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return optionItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvTitle, tvPrice, tvCount;
        private Button btnMinus, btnPlus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tv_option_number_title);
            tvCount = itemView.findViewById(R.id.tv_option_number_count);
            tvPrice = itemView.findViewById(R.id.tv_option_number_price);

            btnMinus = itemView.findViewById(R.id.btn_option_number_minus);
            btnPlus = itemView.findViewById(R.id.btn_option_number_plus);

        }
    }

    public NumberAdapter( ProductDetailActivity activity, ProductOption menuOption ){
        this.activity = activity;
        this.menuOption = menuOption;
        this.optionItems = menuOption.getOptionItems();
    }
}
