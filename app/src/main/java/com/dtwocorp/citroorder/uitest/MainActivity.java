package com.dtwocorp.citroorder.uitest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.dtwocorp.citroorder.uitest.PosMain.view.PosMainActivity;
import com.dtwocorp.citroorder.uitest.ProductDetail.view.ProductDetailActivity;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button btnSearch, btnPayment, btnMoveDetail, btnMoveHome;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMoveHome = findViewById(R.id.btn_move_pos);
        btnMoveHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity( new Intent( getApplicationContext(), PosMainActivity.class ));
            }
        });

        btnMoveDetail = findViewById(R.id.btn_move_product);
        btnMoveDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sendIntent = new Intent( MainActivity.this, ProductDetailActivity.class );
                sendIntent.putExtra("storeId" , 2 );
                sendIntent.putExtra("categoryId", 1);
                sendIntent.putExtra("productId", 1 );
                startActivity( sendIntent );
            }
        });

        btnSearch = findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(
                        () -> {
                            searchDeviceIps();

                        }
                ).start();
                //searchLanDevices();



            }
        });

        btnPayment = findViewById(R.id.btn_payment);
        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(() -> {
                    KiccTcpIp kiccTcpIp = new KiccTcpIp( "220.71.234.114" );
                    String result = kiccTcpIp.openSocket();
                    Log.e("==========", result);

                    kiccTcpIp.sendPaymentRequest("PO");
                }).start();

            }
        });

        searchLanDevices();
    }

    private void searchLanDevices() {
        try {
            WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            if (!wifiManager.isWifiEnabled()) {
                wifiManager.setWifiEnabled(true);
            }
            wifiManager.startScan();

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                List<String> permissions = new ArrayList<>();
                permissions.add( Manifest.permission.ACCESS_FINE_LOCATION );
                ContextCompat.checkSelfPermission( MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION );
                ActivityCompat.requestPermissions(MainActivity.this,permissions.toArray(new String[permissions.size()]),0);
                Log.d("===========", "권한이 필요");
                return;
            }

            List<ScanResult> resultList = wifiManager.getScanResults();

            for( ScanResult result : resultList ){
                Log.d("===========", "SSID " + result.SSID + " BSSID   "
                                + result.BSSID  + "   \n"
                                + result.operatorFriendlyName
                                + "  \n" + result.describeContents()
                                + "   \n" + result.venueName
                        );
            }



        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void searchDeviceIps(){
        try{

            WifiManager wifiManager = ( WifiManager) getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int ipAddress = wifiInfo.getIpAddress();
            String subnet = "220.071.234";
                    //getSubnet(ipAddress);
            try {
                //String subnet = getSubnet(wifiManager.getDhcpInfo().gateway); // 현재 연결된 Wi-Fi 네트워크의 서브넷 마스크를 가져옵니다.
                Log.d("=============subnet", subnet);
                for (int i = 1; i <= 255; i++) {
                    String host = subnet + "." + i;
                    InetAddress address = InetAddress.getByName(host);
                    Log.e("============", String.valueOf(i));
                    if (address.isReachable(1000)) { // 기기가 접속 가능한 상태인지 확인합니다.
                        String hostname = address.getHostName();

                        Log.d("=========", "host name === " + hostname + "  ip === " + address.getHostAddress() +"  \n" + address.getCanonicalHostName() );
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static String getSubnet(int ipAddress) {
        String[] ipParts = new String[3];
        ipParts[0] = Integer.toString((ipAddress >> 0) & 0xff);
        ipParts[1] = Integer.toString((ipAddress >> 8) & 0xff);
        ipParts[2] = Integer.toString((ipAddress >> 16) & 0xff);
        return TextUtils.join(".", ipParts);
    }

  /*  public static String getSubnet(int ipAddress) {
        String[] ipParts = new String[4];
        ipParts[0] = Integer.toString((ipAddress >> 0) & 0xff);
        ipParts[1] = Integer.toString((ipAddress >> 8) & 0xff);
        ipParts[2] = Integer.toString((ipAddress >> 16) & 0xff);
        ipParts[3] = "0";
        return TextUtils.join(".", ipParts);
    }*/
}