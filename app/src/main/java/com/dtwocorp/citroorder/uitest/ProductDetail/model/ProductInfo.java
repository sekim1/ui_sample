package com.dtwocorp.citroorder.uitest.ProductDetail.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductInfo {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("price")
    private int price;

    @Nullable
    @SerializedName("attachment")
    private String attachment;

    @SerializedName("sold_out")
    private boolean soldOut;

    @Nullable
    @SerializedName("stock")
    private int stock;

    @SerializedName("options")
    private ArrayList<ProductOption> options;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    @Nullable
    public String getAttachment() {
        return attachment;
    }

    public boolean isSoldOut() {
        return soldOut;
    }

    public int getStock() {
        return stock;
    }

    public ArrayList<ProductOption> getOptions() {
        return options;
    }
}
