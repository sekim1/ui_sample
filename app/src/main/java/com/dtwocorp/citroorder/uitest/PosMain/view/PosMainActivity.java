package com.dtwocorp.citroorder.uitest.PosMain.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import com.dtwocorp.citroorder.uitest.API.Category.CategoryAPI;
import com.dtwocorp.citroorder.uitest.API.Category.CategoryProductResponseBody;
import com.dtwocorp.citroorder.uitest.API.Category.CategoryResponseBody;
import com.dtwocorp.citroorder.uitest.API.RetrofitBuilder;
import com.dtwocorp.citroorder.uitest.PosMain.adapter.CategoryAdapter;
import com.dtwocorp.citroorder.uitest.PosMain.adapter.ProductAdapter;
import com.dtwocorp.citroorder.uitest.PosMain.model.CategoryInfo;
import com.dtwocorp.citroorder.uitest.PosMain.model.CategoryProductInfo;
import com.dtwocorp.citroorder.uitest.ProductDetail.model.ProductInfo;
import com.dtwocorp.citroorder.uitest.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PosMainActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private CategoryAPI categoryAPI;

    private ArrayList<CategoryInfo> categories;
    private ArrayList<CategoryProductInfo> products;

    private RecyclerView recyclerViewCategory, recyclerViewProduct;

    private CategoryAdapter categoryAdapter;
    private ProductAdapter productAdapter;

    private int selectedCategory = -1;
    private int storeId = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pos_main);

        LinearLayoutManager linearLayout = new LinearLayoutManager( getApplicationContext(), RecyclerView.HORIZONTAL, false );
        GridLayoutManager gridLayoutManager = new GridLayoutManager( getApplicationContext(), 3);

        recyclerViewCategory = findViewById(R.id.recycler_home_category_list);
        recyclerViewProduct = findViewById(R.id.recycler_home_product_list);

        recyclerViewCategory.setLayoutManager( linearLayout );
        recyclerViewProduct.setLayoutManager(gridLayoutManager);

        categories = new ArrayList<>();
        products = new ArrayList<>();

        categoryAdapter = new CategoryAdapter( this,  categories );
        productAdapter = new ProductAdapter( this, products );

        recyclerViewCategory.setAdapter( categoryAdapter );
        recyclerViewProduct.setAdapter( productAdapter );

        retrofit = RetrofitBuilder.getRetrofitClient();
        categoryAPI = retrofit.create(CategoryAPI.class);

        getCategoryList();
    }


    public void getCategoryList(){
        try{

            Call<CategoryResponseBody> categoriesCall = categoryAPI.getCategories( storeId );
            categoriesCall.enqueue(new Callback<CategoryResponseBody>() {
                @Override
                public void onResponse(Call<CategoryResponseBody> call, Response<CategoryResponseBody> response) {
                    try{

                        int resultCode = response.code();
                        if( resultCode >= 200 && resultCode < 300 ){
                            categories.clear();

                            ArrayList<CategoryInfo> tempList = response.body().getData();
                            categories = tempList;
                            categoryAdapter = new CategoryAdapter( PosMainActivity.this,  categories );
                            recyclerViewCategory.setAdapter( categoryAdapter );
                            categoryAdapter.notifyDataSetChanged();

                            if( tempList.size() > 0 ){

                                CategoryInfo first = tempList.get(0);

                                getCategoryProductList( first.getId() );

                            }


                        }else{

                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CategoryResponseBody> call, Throwable t) {

                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getCategoryProductList( int categoryId ){
        try{

            selectedCategory = categoryId;

            Call<CategoryProductResponseBody> productCall = categoryAPI.getCategoryProducts( storeId, categoryId );
            productCall.enqueue(new Callback<CategoryProductResponseBody>() {
                @Override
                public void onResponse(Call<CategoryProductResponseBody> call, Response<CategoryProductResponseBody> response) {
                    try{

                        int resultCode = response.code();

                        if( resultCode >= 200 && resultCode < 300 ){
                            products.clear();

                            ArrayList<CategoryProductInfo> tempList = response.body().getData();
                            products = tempList;
                            productAdapter = new ProductAdapter( PosMainActivity.this, products );
                            recyclerViewProduct.setAdapter( productAdapter );
                            productAdapter.notifyDataSetChanged();

                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CategoryProductResponseBody> call, Throwable t) {

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setCategoryId( int getId ){
        selectedCategory = getId;
    }

    public int getCategoryId(){
        return  selectedCategory;
    }

    public int getStoreId(){
        return  storeId;
    }

}