package com.dtwocorp.citroorder.uitest.PosMain.model;

import com.google.gson.annotations.SerializedName;

public class CategoryProductInfo {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("price")
    private int price;

    @SerializedName("attachment")
    private String attachment;

    @SerializedName("sold_out")
    private boolean soldOut;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getAttachment() {
        return attachment;
    }

    public boolean isSoldOut() {
        return soldOut;
    }
}
