package com.dtwocorp.citroorder.uitest.ProductDetail.model;

import java.util.ArrayList;

public class AddOptionInfo {
    private int id;
    private int price;
    private int count;

    private String perCalcType;

    private String name;
    private ArrayList<Integer> selectedId;

    public AddOptionInfo(int id, int price, int count, String perCalcType, String name, ArrayList<Integer> selectedId) {
        this.id = id;
        this.price = price;
        this.count = count;
        this.perCalcType = perCalcType;
        this.name = name;
        this.selectedId = selectedId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getPerCalcType() {
        return perCalcType;
    }

    public void setPerCalcType(String perCalcType) {
        this.perCalcType = perCalcType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Integer> getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(ArrayList<Integer> selectedId) {
        this.selectedId = selectedId;
    }
}
