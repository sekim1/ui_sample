package com.dtwocorp.citroorder.uitest.PosMain.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class CategoryInfo {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @Nullable
    @SerializedName("attachment")
    private String attachment;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Nullable
    public String getAttachment() {
        return attachment;
    }
}
