package com.dtwocorp.citroorder.uitest.ProductDetail.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductOption {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private String type;

    @SerializedName("required")
    private boolean required;

    @SerializedName("option_items")
    private ArrayList<ProductOptionItem> optionItems;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public boolean isRequired() {
        return required;
    }

    public ArrayList<ProductOptionItem> getOptionItems() {
        return optionItems;
    }

    public ProductOption(int id, String name, String type, boolean required, ArrayList<ProductOptionItem> optionItems) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.required = required;
        this.optionItems = optionItems;
    }
}
