package com.dtwocorp.citroorder.uitest;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

public class KiccTcpIp {

    private Socket socket;
    private OutputStream outputStream;
    private InputStream inputStream;

    private String deviceIp;

    private Thread mainThread;

    // 220.71.234.114
    public KiccTcpIp( String deviceIp ){
        this.deviceIp = deviceIp;
    }

    public String openSocket(){
        try{
            socket = new Socket( deviceIp, 13189);

            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();

           mainThread = new Thread(new SocketListener());
           mainThread.start();

        }catch (UnknownHostException unknownHostException){
            unknownHostException.printStackTrace();
            return "ERROR: host not find";
        }catch (IOException ioException){
            ioException.printStackTrace();
            return "ERROR: network not responding";
        }catch (SecurityException securityException){
            securityException.printStackTrace();
            return  "ERROR: security access denied";
        }catch(IllegalArgumentException illegalArgumentException){
            illegalArgumentException.printStackTrace();
            return "ERROR: Illegal Argument";
        }
        catch (Exception e){
            e.printStackTrace();
            return "error: connection failed";
        }

        return "success";
    }

    public void sendPaymentRequest( String msg ){
        try{
            /*String sendMsg = "\u00020xFD" + msg +"\u0003";
            outputStream.write(  sendMsg.getBytes(Charset.forName("US-ASCII") ));*/

            byte[] packet = new byte[9];
            packet[0] = 0x02; // STX
            packet[1] = 0x00; // LEN
            packet[2] = 0x10; // LEN
            packet[3] = 0x00; // CNT
            packet[4] = (byte) 0xFD; // CMD
            packet[5] = 0x50; // DATA P   0x50 0x4F
            packet[6] = 0x4F; // DATA O
            packet[7] = 0x03; // ETX
            packet[8] = (byte)(packet[0] ^ packet[1] ^ packet[2] ^ packet[3] ^ packet[4] ^ packet[5]); // LRC

            for(int i = 0 ; i < packet.length ; i++ ){
                String ms = String.valueOf(packet[i]);
                Log.e("=========", i + "    " + ms);

            }

            outputStream.write(packet);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void closeSocket(){
        try {
            Log.d("==========","closeSocket");
            outputStream.close();
            inputStream.close();

            socket.close();
            mainThread.interrupt();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            socket = null;
            outputStream = null;
            inputStream = null;
        }
    }

    private class SocketListener implements  Runnable {

        @Override
        public void run() {
            try{
                byte[] buffer = new byte[1024];
                int bytesRead = inputStream.read( buffer );

                byte[] response = Arrays.copyOf( buffer, bytesRead );
                String responseDataString = new String(response);

                Log.e("=========message", responseDataString+" \n"  + byteArrayToHex( buffer )  );

            }catch (IOException ioException ){
                ioException.printStackTrace();
            }
            catch (Exception e){
                e.printStackTrace();

            }finally {
                closeSocket();
            }


        }

    }

    public String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder();
        for(final byte b: a)
            sb.append(String.format("%02x ", b&0xff));
        return sb.toString();
    }



}
